package com.sourceit.example.testwriter;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sourceit.example.testwriter.Utils.isExternalStorageWritable;

public class MainActivity extends AppCompatActivity {

    public static final String DIR_NAME = "testDir";
    public static final String FILE_NAME = "myFile.txt";

    @BindView(R.id.am_login)
    EditText login;
    @BindView(R.id.am_pass)
    EditText pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loadIfExists();

        if (!isPermissionsGranted()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
            //resume tasks needing this permission



        }
    }

    private void loadIfExists() {
        File sdCard = Environment.getExternalStorageDirectory(); // create file with contains path to external storage
        File directory = new File(sdCard, DIR_NAME); // create file with contains path to MyDir folder
        File file = new File(directory, FILE_NAME); // create file with contains path to myFile.txt file
        if (file.exists()) {
            //read file
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                login.setText(br.readLine());
                pass.setText(br.readLine());
                br.close(); //important: close reader
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.am_action_save)
    void onSaveClick() {
        if (isExternalStorageWritable()) {
            //create dirs
            File sdCard = Environment.getExternalStorageDirectory();
            File directory = new File (sdCard, DIR_NAME);
            directory.mkdirs();

            //create file
            File file = new File(directory, FILE_NAME);

            try {
                //write data to file
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);
                osw.write(login.getText().toString());
                osw.write("\n");
                osw.write(pass.getText().toString());
                osw.flush();
                osw.close();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

        }
    }

    public boolean isPermissionsGranted() {
        //check API version
        if (Build.VERSION.SDK_INT >= 23) {
            //check permission
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //if all right
                return true;
            } else {
                //if permission not granted
                return false;
            }

        } else {
            //permission available as is
            return true;
        }
    }
}
