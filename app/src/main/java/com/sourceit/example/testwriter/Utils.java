package com.sourceit.example.testwriter;

import android.os.Environment;

/**
 * Created by wenceslaus on 30.05.17.
 */

public final class Utils {

    private Utils() {
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
